# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import logging
import datetime

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)

import sys
import os
import csv


class DBBank2FreeAgent:
    def __init__(self, input_filename, output_filename):
        self.input_filename = input_filename
        self.output_filename = output_filename

    def convert(self):
        log.info("Converting %s", self.input_filename)

        with open(self.input_filename, 'rb') as csvfile:
            numline = len(csvfile.readlines())

        with open(self.input_filename, 'rb') as csvfile:
            dbreader = csv.reader(csvfile, delimiter=str(";"))


            with open(self.output_filename, 'wb') as freeagentfile:
                freeagentwriter = csv.writer(freeagentfile, delimiter=str(','))

                rows_written = 0
                row_index = 0
                for row in dbreader:
                    row_index += 1

                    if row_index <= 5 or row_index == numline:
                        continue
                    print row
                    date = row[0]
                    date = datetime.datetime.strptime(date, "%m/%d/%Y")
                    date = date.strftime('%d/%m/%Y')

                    description = " - ".join([row[3].decode('cp1252'), row[4].decode('cp1252')]).encode('utf-8')

                    debit = row[15]
                    credit = row[16]

                    if debit:
                        amount = debit
                    else:
                        amount = credit

                    amount = str(float(amount.replace(',', '')))

                    data = [date, amount, description]
                    freeagentwriter.writerow(data)
                    rows_written += 1

        log.info("Found %d statements", rows_written)
        log.info("Saved to \"%s\"", self.output_filename)


if __name__ == "__main__":
    input_filename = sys.argv[1]
    output_filename = "{0}.freeagent.csv".format(os.path.basename(input_filename))
    output_filename = os.path.join(os.path.dirname(input_filename), output_filename)

    converter = DBBank2FreeAgent(input_filename, output_filename)
    converter.convert()