# Converter from Deutsche Bank CSV format to FreeAgent compatible CSV

The script here convers `CSV` bank statement that you can download from German version of Deutsche Bank Online 
Banking system and outputs `CSV` that is acceptable by `FreeAgent` software.

## Usage

    python convert.py INPUT_FILE 
    
The script will output `INPUT_FILE_NAME.fileagent.csv` file in the same directory as source file